# mandatory-1

## Project setup, compilation and hot-reloads for development

```
docker-compose up
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
